import 'core-js/es7';
import 'reflect-metadata';
require('zone.js/dist/zone');
/// <reference path='../node_modules/@types/node/index.d.ts' />
if (process.env.ENV === 'production') {
  // Production
} else {
  // Development
  //Error[0] = Infinity;
  require('zone.js/dist/long-stack-trace-zone');
}
