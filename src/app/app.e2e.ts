/**
 * Created by js on 02-11-2016.
 */
///<reference path='../../node_modules/@types/jasmine/index.d.ts' />
///<reference path='../../node_modules/@types/protractor/index.d.ts' />
describe('App', () => {
    beforeAll(() => {
        browser.get('/');
    });
    it('should have a title', () => {
        let subject = element(by.tagName('ar-app')).isPresent();
        expect(subject).toBeTruthy();
    });
});
