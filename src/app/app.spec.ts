import {AppComponent} from './app.component';
import {AppModule} from './app.module';
import {TestBed, inject} from '@angular/core/testing';
import {TranslateService, TranslateLoader} from 'ng2-translate';
describe('App', () => {
    // provide our implementations or mocks to the dependency injector
    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            AppComponent,
            AppModule,
            TranslateService,
            TranslateLoader
        ]
    }));
    it('should have a component', inject([AppComponent], (appComponent: AppComponent) => {
        expect(appComponent).toBeDefined();
    }));
});
