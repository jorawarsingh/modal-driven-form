/**
 * Created by js on 25-10-2016.
 */
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {AppRoutingModule} from "./app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

export const VendorModules = [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule
];
export const ARModules = [
    AppRoutingModule
];
