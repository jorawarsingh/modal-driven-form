import {AppComponent} from "./app.component";
import {NgModule} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {VendorModules} from "./app-modules";

@NgModule({
    bootstrap: [AppComponent],
    declarations: [AppComponent],
    imports: [...VendorModules],
    providers: [
        Title,
    ]
})
export class AppModule {
}
