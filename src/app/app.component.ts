import {Component, OnInit, OnDestroy} from "@angular/core";
import { trigger, state, style, transition, animate} from "@angular/animations";
import {TranslateService} from "ng2-translate";
//import {MdDialog, MdDialogConfig} from "@angular/material";
import {NavigationEnd, Router} from "@angular/router";
@Component({
    selector: 'ar-app',
    styleUrls: ['app.styles.css'],
    templateUrl: 'app.template.html',
    animations: [
        trigger('slideInOutLeft', [
            state('in-left', style({
                transform: 'translate3d(0%, 0, 0)'
            })),
            state('out-left', style({
                transform: 'translate3d(-80%, 0, 0)'
            })),
            transition('in-left => out-left', animate('400ms ease-in-out')),
            transition('out-left => in-left', animate('400ms ease-in-out'))
        ])
    ]
})

export class AppComponent implements OnInit, OnDestroy {

    ngOnDestroy(): void {

    }

    constructor() {
    }


    ngOnInit(): void {

    }


}
