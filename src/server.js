var express = require('express');
var fallback = require('express-history-api-fallback')
var app = express();
var path = require('path');
var root = __dirname + '/'
app.use('/', express.static(__dirname + "/"));
app.use(fallback('index.html', { root: root }))

app.listen(8080, function () {
    console.log('Example app listening on port 8080!');
});