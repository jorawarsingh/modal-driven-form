import { AppModule } from './app/app.module';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import 'bootstrap-css-only/css/bootstrap.css';
require('font-awesome/css/font-awesome.css');
import 'primeng/resources/primeng.min.css';
import 'primeng/resources/themes/omega/theme.css';
import "./index.css";

/// <reference path='../node_modules/@types/node/index.d.ts' />

if (process.env.NODE_ENV === 'production') { enableProdMode(); }
platformBrowserDynamic().bootstrapModule(AppModule);
