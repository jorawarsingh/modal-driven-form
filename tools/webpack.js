const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const ngtools = require('@ngtools/webpack');
module.exports = function (env) {
    const config = {
        entry: {
            polyfill: './src/polyfills.ts',
            vendor: './src/vendors.ts',
            main: './src/main.aot.ts'
        },
        resolve: {
            extensions: [".js", ".json", ".ts"]
        },
        output: {
            filename: '[name].js',
            path: path.resolve('dist')
        },
        module: {
            rules: [
                {
                    test: /\.ts?$/,
                    use: '@ngtools/webpack',
                    exclude: [/node_modules\/(?!(ng2-.+|ngx-.+))/]
                },
                {
                    test: /\.css$/,
                    exclude:'node_modules',
                    include: path.resolve(process.cwd(), 'src', 'app'),
                    use: ['to-string-loader', 'css-loader']
                },
                /*{
                    test: /\.css$/,
                    exclude: [path.resolve(process.cwd(), 'src', 'app'),path.resolve(process.cwd(), 'node_modules')],
                    use: ExtractTextPlugin.extract
                    ({
                        fallback: 'style-loader',
                        use: ['to-string-loader', 'css-loader?sourceMap']
                    })
                },*/
                {
                    test: /\.html/,
                    use: "html-loader"
                },
                {
                    test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    use: "url-loader?limit=10000&mimetype=application/font-woff"
                },
                {
                    test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    use: "file-loader"
                },
                {
                    test: /\.(png|jpg|jpeg|gif)$/,
                    use: 'url-loader?limit=10000'
                },
                {
                    test: /\.json$/,
                    use: "json-loader"
                }
            ]
        },
        plugins: [new HtmlWebpackPlugin({
            template: "./src/index.html",
            chunksSortMode: function (a, b) {  //alphabetical order
                if (a.names[0] < b.names[0]) {
                    return 1;
                }
                if (a.names[0] > b.names[0]) {
                    return -1;
                }
                return 0;
            }
        }),
            new ExtractTextPlugin("styles.css"),
            new webpack.ContextReplacementPlugin(
                /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
                __dirname
            ), new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false,
                    drop_console: true,
                }
            }),
            new ngtools.AotPlugin({
                tsConfigPath: path.join(process.cwd(), 'tsconfig.json'),
                entryModule: path.join(process.cwd(), 'src/app/app.module#AppModule')
            })],
        devtool: "source-map",
        target: "web",
        stats: "errors-only",
        watch: true,
        watchOptions: {
            aggregateTimeout: 1000,

            poll: true,
            poll: 500,
        },
        devServer: {
            contentBase: path.join(__dirname, "dist"),
            compress: true,
            port: 8080,
            clientLogLevel: "none",
            historyApiFallback: true
        }
    };
    return config;
};

